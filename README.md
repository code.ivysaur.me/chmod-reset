# chmod-reset

![](https://img.shields.io/badge/written%20in-bash-blue)

Reset file permissions via file introspection.

When migrating between Cygwin and Linux proper, or when FAT/NTFS filesystems are in use, permissions may be lost. This script makes a best effort to reset file permissions to something "normal" by performing deep file introspection to determine whether a file is executable.

Tags: sysadmin

## Usage


```
About:
   chmod-reset can reset file and directory permissions to something "normal".
   That's 755 for directories and executables, and 644 for other files.

Usage:
   chmod-reset [OPTIONS] [--] DIRECTORY [ DIRECTORY ... ]

Options:
   -f, --exec-check-fast   Mark files executable based on extension
   -s, --exec-check-slow   Mark files executable based on content (default)
   --help                  Display this message and exit
```


## Changelog

2017-04-23 1.0
- Initial public release
- [⬇️ chmod-reset-1.0.tar.xz](dist-archive/chmod-reset-1.0.tar.xz) *(1016B)*

